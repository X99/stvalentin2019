#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <math.h>
#include <Adafruit_NeoPixel.h>

//LEFT = indice pour la partie gauche, RIGHT pour la partie droite.
#define LEFT 0
#define RIGHT 1

#define NPIXELS 5   //nombre de pixels
#define PINL    9   //pin à laquelle est connectée la patte DIN du ruban de gauche
#define PINR    10  //idem pour le ruban de droite

//tableau contenant les instances, une par côté de manière à pouvoir contrôler les deux indépendemment.
Adafruit_NeoPixel strips[2] = {
  Adafruit_NeoPixel(NPIXELS, PINL, NEO_GRB + NEO_KHZ800),   //left
  Adafruit_NeoPixel(NPIXELS, PINR, NEO_GRB + NEO_KHZ800)    //right
};

//char auth[] = "e0498cb906674504901f63060c584f8d";
//char ssid[] = "canette24";
//char pass[] = "moTunkKd2JyMU";
char auth[] = "7d8cf27507f54192888d1b81baa4b0f6"; //toekn généré par le projet Blynk sur le smartphone
char ssid[] = "TahaLAN";      //SSID du réseau WiFi auquel se connecte l'ESP
char pass[] = "TahaLAN99";    //clé du réseau WiFi 

//Valeurs de la couleur actuelle, une pour le côté gauche, une pour le côté droit
uint8_t r[2] = {0};
uint8_t g[2] = {0};
uint8_t b[2] = {0};
//quand isShowing = true, l'effet de "respiration" s'affiche
bool isShowing[2] = {false};
//moment à partir duquel on a lancé l'effet. Sert à savoir quand l'arrêter
uint32_t startTime[2] = {0};
unsigned long breathe_time[2] = {millis(), millis()};
//délai entre deux tours de boucle de l'animation "respiration". Plus la valeur est faible, plus la respiration est rapide
int breathe_delay = 10;
//durée de l'effet de respiration, en millisecondes
uint32_t duration = 13.1 * 1000L;

#define EFFECT_START_VALUE  100
int i[2]={EFFECT_START_VALUE};

//fonction appelée lorsque la variable V0 est modifiée dans l'application Blynk
BLYNK_WRITE(V0) {
  r[LEFT] = (uint8_t)param[0].asInt();
  g[LEFT] = (uint8_t)param[1].asInt();
  b[LEFT] = (uint8_t)param[2].asInt();
  isShowing[LEFT] = true;
}

BLYNK_WRITE(V1) {
  r[RIGHT] = (uint8_t)param[0].asInt();
  g[RIGHT] = (uint8_t)param[1].asInt();
  b[RIGHT] = (uint8_t)param[2].asInt();
  isShowing[RIGHT] = true;
}


void setup() {
  Serial.begin(115200);
 
  Blynk.begin(auth, ssid, pass);

  //initialisation des deux rubans de LED
  for(uint8_t side=0; side<2; side++) {
    strips[side].begin();
    strips[side].Color(0, 0, 0);
    strips[side].setBrightness(0);
    strips[side].show();
  }
}
 
void loop() {
  Blynk.run();

  //ce passage permet de lancer l'effet pour un certain temps.
  //lorsque le temps est écoulé, on conserve la couleur avec la luminosité au max.
  for(uint8_t side=0; side<2; side++) {
    if (isShowing[side] == true) {
      if (startTime[side] == 0) {
        startTime[side] = millis();
        i[side]=EFFECT_START_VALUE;
      }
      //on vérifie ici si le temps aloué est écoulé.
      if ((millis() - startTime[side]) < duration) {
        nonBlockingBreath(side);
      } else {
        startTime[side] = 0;
        isShowing[side] = false;
      }
    }
  }
}


//cette fonction gère l'effet de "respiration" en calculant, à partir d'une fonction
//à base sinusoïdale.
//Source: https://arduinoelectronics.wordpress.com/2015/05/12/non-blocking-breathing-led/
void nonBlockingBreath(uint8_t side) {
  if( (breathe_time[side] + breathe_delay) < millis() ){
    breathe_time[side] = millis();
    float val = (exp(sin(i[side]/2000.0*PI*10)) - 0.36787944)*108.0;
    
    for (int n=0; n<NPIXELS; n++) {
      strips[side].setPixelColor(n, strips[side].Color(r[side], g[side], b[side]));
    }
    //Serial.println(String(i[side]) + "-" + String(val));
    strips[side].setBrightness(val);
    strips[side].show();
    i[side]++;
  }
}
